var preparing = 0;

$(document).ready(function() {
	function resizeDynamicTable(index, element) {
		preparing++;
		element           = $(element);
		var headerRowsDOM = element.find("thead tr th");
		var bodyRowsDOM   = element.find("tbody tr td");
		//region Resetta le dimensioni
		headerRowsDOM.css("width", "");
		bodyRowsDOM.css("width", "");
		//endregion
		var rows        = [];
		var columnCount = headerRowsDOM.length;
		//region Ottiene tutte le larghezze delle colonne e le ridimensiona
		headerRowsDOM.each(function(index, elem) {
			elem              = $(elem);
			rows[rows.length] = {
				header: elem,
				width: parseInt(elem.css("width"))
			}
		});
		bodyRowsDOM.each(function(index, elem) {
			elem = $(elem);
			if(parseInt(elem.css("width")) > rows[index % columnCount].width) rows[index % columnCount].widht = parseInt(elem.css("width"));
			elem.css("width", rows[index % columnCount].width);
		});
		headerRowsDOM.each(function(index, elem) {
			elem = $(elem);
			elem.css("width", rows[index % columnCount].width);
		});
		//endregion
		preparing--;
	}

	$(window).resize(function() {
		$(".dynamic.table").each(resizeDynamicTable);
	});
});

//region Triggera tutte le funzioni necessarie
$(document).ready(function() {
	$(window).trigger("resize");
	setTimeout(function() {
		loadingComplete()
	}, 100);
});
function loadingComplete() {
	if(preparing == 0) {
		console.log("ready");
		$(".loading-panel").remove();
	}
	else {
		setTimeout(function() {
			loadingComplete();
		}, 100);
	}
}
//endregion