$(document).ready(function() {
	$(".delete a").click(function(evt) {
		var element = $(evt.currentTarget);
		var id      = element.data("id");
		$.ajax({
			url: "/" + appLocale + "/admin/plugins/" + id,//TODO route
			method: "DELETE",
			complete: function(xhr) {
				if(xhr.status == 200) {
					toastr.success(xhr.responseText);
					element.parents("tr").remove();
					//TODO refresh table pagination
				}
				else if(xhr.status == 500) {
					toastr.error("Errore tecnico");//TODO trans
				}
				else {
					toastr.error(xhr.responseText);
				}
			}
		})
	});
});