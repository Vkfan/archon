<?php
namespace AppBundle\Form;

use AppBundle\Entity\Plugin;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\Translator;

class PluginType extends AbstractType {
	/** @var EntityManager $em */
	private $em;
	/** @var Translator $trans */
	private $trans;
	public function __construct(EntityManager $em, Translator $trans) {
		$this->em = $em;
		$this->trans = $trans;
	}

	public function buildForm(FormBuilderInterface $builder, array $options) {
		$dependablePlugins = $this->getDependablePlugins($options["data"]);

		$builder
			->add("id", HiddenType::class)
			->add("name", TextType::class, [
				"attr"               => [
					"placeholder" => "plugin.name"
				],
				"label"              => "plugin.name",
				"translation_domain" => "form"
			])
			->add("css", CollectionType::class, [
				"allow_add"    => true,
				"allow_delete" => true,
				"delete_empty" => true,
				"entry_type"   => TextType::class,
				"required"     => false,
				"prototype"    => true
			])
			->add("js", CollectionType::class, [
				"allow_add"    => true,
				"allow_delete" => true,
				"delete_empty" => true,
				"entry_type"   => TextType::class,
				"required"     => false,
				"prototype"    => true
			])
			->add("dependencies", EntityType::class, [
				"choice_label" => "name",
				"choices"      => $dependablePlugins,
				"class"        => "AppBundle\\Entity\\Plugin",
				"multiple"     => true,
				"required"     => false
			])
			->add("submit", ButtonType::class);
	}

	/**
	 * @since   0.1.0
	 * @version 0.1.0
	 *
	 * @param Plugin $plugin
	 *
	 * @return Plugin[]
	 */
	public function getDependablePlugins(Plugin $plugin) {
		$qb = $this->em->createQueryBuilder()
			->select("p")
			->from("AppBundle:Plugin", "p")
			->where("p.status != :status")
			->setParameter("status", Plugin::STATUS_DELETED);
		if($plugin->getId() !== null) {
			$qb
				->andWhere("p.id != :id")
				->setParameter("id", $plugin->getId());
		}

		return $qb->getQuery()->getResult();
	}
}