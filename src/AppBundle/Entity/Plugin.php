<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Plugin
 *
 * @todo    add validation messages
 *
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PluginRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name = "plugin")
 */
class Plugin {
	const STATUS_ACTIVE = 1;
	const STATUS_DELETED = 2;

	/**
	 * @ORM\PrePersist
	 */
	public function checkFolderName() {
		if($this->id === null) {
			$baseName = $this->folderName;
			if(strcmp("", $baseName) === 0) $baseName = str_replace(" ", "-", strtolower($this->name));

			$count = 1;
			$found = false;
			$name = "";
			while(!$found) {
				$name = $baseName;
				if($count > 1) $name .= "-" . $count;

				if(!file_exists("lib/" . $name)) $found = true;
				else $count++;
			}
			$this->folderName = $name;
			mkdir("lib/" . $this->folderName);
		}
	}

	/**
	 * @ORM\Id
	 * @ORM\Column(type = "bigint")
	 * @ORM\GeneratedValue(strategy = "AUTO")
	 * @var int $id
	 */
	private $id;
	/**
	 * @ORM\Column(type = "text")
	 * @Assert\NotBlank()
	 * @var string $name
	 */
	private $name;
	/**
	 * @ORM\Column(type = "text")
	 * @var String $folderName
	 */
	private $folderName;
	/**
	 * @ORM\Column(type = "smallint")
	 * @var integer $status
	 * 1: Active
	 * 2: Deleted
	 */
	private $status;

	/**
	 * Lista di file css (comma separated) che compongono il plugin
	 * @ORM\Column(type = "array")
	 * @Assert\All({
	 *     @Assert\Regex("/\.css$/")
	 * })
	 *
	 * @var string $css
	 */
	private $css;
	/**
	 * Lista di file js (comma separated) che compongono il plugin
	 * @ORM\Column(type = "array")
	 * @Assert\All({
	 *     @Assert\Regex("/\.js$/")
	 * })
	 *
	 * @var string $js
	 */
	private $js;
	/**
	 * @since   0.1.0
	 * @version 0.1.0
	 * @return bool
	 * @Assert\IsTrue()
	 */
	public function hasCssOrJs() {
		return count($this->css) || count($this->js);
	}

	/**
	 * @ORM\ManyToMany(targetEntity = "AppBundle\Entity\Plugin")
	 * @ORM\JoinTable(name = "plugin_dependency",
	 *    joinColumns = {@ORM\JoinColumn(name = "id_figlio", referencedColumnName = "id", onDelete = "CASCADE")},
	 *    inverseJoinColumns = {@ORM\JoinColumn(name = "id_padre", referencedColumnName = "id")})
	 */
	private $dependencies;
	/**
	 * Constructor
	 */
	public function __construct() {
		$this->status = 1;
		$this->dependencies = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Plugin
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set css
	 *
	 * @param string $css
	 *
	 * @return Plugin
	 */
	public function setCss($css) {
		$this->css = $css;

		return $this;
	}

	/**
	 * Get css
	 *
	 * @return string
	 */
	public function getCss() {
		return $this->css;
	}

	/**
	 * Set js
	 *
	 * @param string $js
	 *
	 * @return Plugin
	 */
	public function setJs($js) {
		$this->js = $js;

		return $this;
	}

	/**
	 * Get js
	 *
	 * @return string
	 */
	public function getJs() {
		return $this->js;
	}

	/**
	 * Add dependency
	 *
	 * @param \AppBundle\Entity\Plugin $dependency
	 *
	 * @return Plugin
	 */
	public function addDependency(\AppBundle\Entity\Plugin $dependency) {
		$this->dependencies[] = $dependency;

		return $this;
	}

	/**
	 * Remove dependency
	 *
	 * @param \AppBundle\Entity\Plugin $dependency
	 */
	public function removeDependency(\AppBundle\Entity\Plugin $dependency) {
		$this->dependencies->removeElement($dependency);
	}

	/**
	 * Get dependencies
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getDependencies() {
		return $this->dependencies;
	}

	/**
	 * Set folderName
	 *
	 * @param string $folderName
	 *
	 * @return Plugin
	 */
	public function setFolderName($folderName) {
		$this->folderName = $folderName;

		return $this;
	}

	/**
	 * Get folderName
	 *
	 * @return string
	 */
	public function getFolderName() {
		return $this->folderName;
	}

	/**
	 * Set status
	 *
	 * @param integer $status
	 *
	 * @return Plugin
	 */
	public function setStatus($status) {
		$this->status = $status;

		return $this;
	}

	/**
	 * Get status
	 *
	 * @return integer
	 */
	public function getStatus() {
		return $this->status;
	}
}
