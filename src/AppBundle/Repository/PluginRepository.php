<?php
namespace AppBundle\Repository;

use AppBundle\Entity\Plugin;
use Doctrine\ORM\EntityRepository;

class PluginRepository extends EntityRepository {
	/**
	 * @since   0.1.0
	 * @version 0.1.0
	 *
	 * @param Plugin $plugin
	 *
	 * @return bool
	 */
	public function checkDuplication($plugin) {
		$em = $this->getEntityManager();
		//region Name
		$collision = $em->getRepository("AppBundle:Plugin")->findOneBy([
			"name" => $plugin->getName()
		]);
		if($collision !== null && ($plugin !== null && $collision->getId() !== $plugin->getId())) return false;//TODO generate error
		//endregion

		return true;
	}
}