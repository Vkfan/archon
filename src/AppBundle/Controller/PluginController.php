<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Plugin;
use AppBundle\Form\PluginType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PluginController
 *
 * @author  Vkfan
 * @package AppBundle\Controller
 * @version 0.1.0
 * @Route("/{_locale}/admin/plugins")
 */
class PluginController extends Controller {
	/**
	 * @since   0.1.0
	 * @version 0.1.0
	 *
	 * @return Response
	 * @Route("", name="getAllPlugins")
	 */
	public function getAllPluginsAction() {
		//TODO controllo permessi
		$em = $this->getDoctrine()->getManager();
		$plugins = $em->getRepository("AppBundle:Plugin")->findBy([
			"status" => 1
		], ["name" => "ASC"]);

		return $this->render("archon/plugins/plugins.twig", [
			"plugins" => $plugins
		]);
	}

	/**
	 * @since   0.1.0
	 * @version 0.1.0
	 * @return Response
	 * @Route("/nuovo",name="createPluginPage")
	 * @Method("GET")
	 */
	public function createPluginPageAction() {
		// TODO controlla permessi
		$plugin = new Plugin();
		$form = $this->createForm(PluginType::class, $plugin);

		return $this->render("archon/plugins/plugin.twig", [
			"form" => $form->createView()
		]);
	}

	/**
	 * @since   0.1.0
	 * @version 0.1.0
	 *
	 * @param Request $req
	 * @param int     $id
	 *
	 * @return Response|RedirectResponse
	 * @Route("/nuovo",name="createPlugin")
	 * @Route("/{id}",name="editPlugin", requirements={"id":"^\d+$"})
	 * @Method({"POST","PUT"})
	 */
	public function createPluginAction(Request $req, $id = null) {
		$em = $this->getDoctrine()->getManager();

		$plugin = null;
		if($id !== null) $plugin = $em->getRepository("AppBundle:Plugin")->findOneBy([
			"id" => $id
		]);
		if($plugin === null) $plugin = new Plugin();

		$form = $this->createForm(PluginType::class, $plugin);
		$form->handleRequest($req);

		if($form->isSubmitted() && $form->isValid()) {
			$res = $em->getRepository("AppBundle:Plugin")->checkDuplication($plugin);

			if($res) {
				$em->persist($plugin);
				$em->flush();

				if($id === null) {
					return new Response($this->generateUrl("editPluginPage", [
						"id" => $plugin->getId()
					]), 301);
				}
				else return new Response("Modifica completata");//TODO trans
			}
			else {
				return new Response("Nome duplicato", 400);//TODO genera errore
			}
		}

		return new Response("");
	}

	/**
	 * @since   0.1.0
	 * @version 0.1.0
	 *
	 * @param int $id
	 *
	 * @return Response
	 * @Route("/{id}",name="editPluginPage",requirements={"id":"^\d+$"})
	 * @Method("GET")
	 */
	public function editPluginPageAction($id) {
		// TODO controlla permessi
		$plugin = $this->getDoctrine()->getManager()->getRepository("AppBundle:Plugin")->findOneBy([
			"id" => $id
		]);
		$form = $this->createForm(PluginType::class, $plugin, [
		]);

		return $this->render("archon/plugins/plugin.twig", [
			"form" => $form->createView()
		]);
	}

	/**
	 * @since   0.1.0
	 * @version 0.1.0
	 *
	 * @param int $id
	 *
	 * @return Response
	 * @Route("/{id}",name="deletePlugin",requirements={"id":"^\d+"})
	 * @Method("DELETE")
	 */
	public function deletePluginAction($id) {
		// TODO controlla permessi
		$em = $this->getDoctrine()->getManager();
		$plugin = $em->getRepository("AppBundle:Plugin")->findOneBy([
			"id" => $id
		]);
		if($plugin === null) return new Response("Non trovato", 400);//TODO trans
		$plugin->setStatus($plugin::STATUS_DELETED);
		$em->persist($plugin);
		$em->flush();

		return new Response("Successo");//TODO trans
	}
}