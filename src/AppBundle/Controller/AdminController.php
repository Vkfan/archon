<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller {
	/**
	 * @since 0.1.0
	 * @version 0.1.0
	 * @param Request $request
	 * @Route("/admin", name="homepage")
	 */
	public function indexAction(Request $request) {

	}
}